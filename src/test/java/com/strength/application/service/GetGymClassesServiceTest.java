package com.strength.application.service;

import static org.mockito.Mockito.when;


import com.strength.application.port.out.GetGymClassesPort;
import com.strength.domain.GymClass;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import java.util.List;
import javax.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.wildfly.common.Assert;

@QuarkusTest
public class GetGymClassesServiceTest {

  @Inject
  private GetGymClassesService getGymClassesService;

  @InjectMock
  private GetGymClassesPort getGymClassesPort;

  @BeforeEach
  void setUp() {
    when(getGymClassesPort.findAll())
        .thenReturn(List.of(new GymClass()));
  }

  @Test
  public void testFindAllGymClasses() {
    List<GymClass> allGymClasses = getGymClassesService.findAll();

    Assert.assertTrue(allGymClasses.size() == 1);
  }
}
