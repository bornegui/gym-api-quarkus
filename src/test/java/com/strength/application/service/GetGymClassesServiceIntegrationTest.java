package com.strength.application.service;

import com.strength.domain.GymClass;
import io.quarkus.test.junit.QuarkusTest;
import java.util.List;
import javax.inject.Inject;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@QuarkusTest
public class GetGymClassesServiceIntegrationTest {

  @Container
  public static PostgreSQLContainer pg = new PostgreSQLContainer<>("postgres:12.4")
      .withDatabaseName("gymDB")
      .withUsername("gym")
      .withPassword("gym")
      .withExposedPorts(5432);
  @Inject
  private CreateGymClassService createGymClassService;
  @Inject
  private GetGymClassesService getGymClassesService;

  @BeforeEach
  public void setUp() {
    GymClass gymClass = new GymClass();
    createGymClassService.createGymClass(gymClass);
  }

  @Test
  public void testFindAllGymClasses() {
    List<GymClass> gymClasses = getGymClassesService.findAll();
    Assert.assertEquals(1, gymClasses.size());
  }

}
