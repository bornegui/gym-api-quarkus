package com.strength.adapter.in.web;

import com.strength.application.port.in.CreateGymClassUseCase;
import com.strength.application.port.in.GetGymClassesUseCase;
import com.strength.application.port.in.RetrieveGymClassUseCase;
import com.strength.domain.GymClass;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/classes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class GymClassController {

  @Inject
  GetGymClassesUseCase getGymClasses;

  @Inject
  RetrieveGymClassUseCase retrieveGymClassUseCase;

  @Inject
  CreateGymClassUseCase createGymClassUseCase;

  @POST
  public void createGymClass(GymClass gymClass) {
    createGymClassUseCase.createGymClass(gymClass);
  }

  @GET
  public List<GymClass> getClasses() {
    return getGymClasses.findAll();
  }

  @GET
  @Path("{id}")
  public GymClass getClass(@PathParam("id") String id) {
    return retrieveGymClassUseCase.findById(Long.valueOf(id));
  }
}
