package com.strength.adapter.out.persistence.jpa.adapter;

import com.strength.adapter.out.persistence.jpa.model.GymClass;
import com.strength.adapter.out.persistence.jpa.repository.GymClassesJpaRepository;
import com.strength.application.port.out.CreateGymClassPort;
import com.strength.application.port.out.GetGymClassesPort;
import com.strength.application.port.out.RetrieveGymClassPort;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class GymClassesJpaAdapter implements GetGymClassesPort, RetrieveGymClassPort, CreateGymClassPort {

  @Inject
  GymClassesJpaRepository gymClassesJpaRepository;

  @Override
  @Transactional
  public void createGymClass(com.strength.domain.GymClass gymClass) {
    GymClass gymClassEntity = new GymClass();
    gymClassEntity.setName(gymClass.getName());
    gymClassesJpaRepository.persist(gymClassEntity);
  }

  @Override
  public List<com.strength.domain.GymClass> findAll() {
    List<GymClass> gymClasses = gymClassesJpaRepository.listAll();
    return gymClasses.stream().map(gymClass -> {
      com.strength.domain.GymClass domainGymClass = new com.strength.domain.GymClass();
      domainGymClass.setId(gymClass.getId());
      domainGymClass.setName(gymClass.getName());
      return domainGymClass;
    }).collect(Collectors.toList());
  }

  @Override
  public com.strength.domain.GymClass findById(Long id) {
    GymClass gymClass = gymClassesJpaRepository.findById(id);
    com.strength.domain.GymClass domainGymClass = new com.strength.domain.GymClass();
    domainGymClass.setId(gymClass.getId());
    domainGymClass.setName(gymClass.getName());
    return domainGymClass;
  }
}
