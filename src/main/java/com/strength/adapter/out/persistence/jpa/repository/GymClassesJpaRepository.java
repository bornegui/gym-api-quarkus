package com.strength.adapter.out.persistence.jpa.repository;

import com.strength.adapter.out.persistence.jpa.model.GymClass;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GymClassesJpaRepository implements PanacheRepository<GymClass> {
}
