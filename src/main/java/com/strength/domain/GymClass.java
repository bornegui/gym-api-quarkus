package com.strength.domain;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GymClass {

  private Long id;

  private String name;

  private int capacity;

  private LocalDate startDate;

  private LocalDate endDate;

}

