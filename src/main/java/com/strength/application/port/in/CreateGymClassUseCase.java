package com.strength.application.port.in;

import com.strength.domain.GymClass;

public interface CreateGymClassUseCase {

  void createGymClass(GymClass gymClass);
}
