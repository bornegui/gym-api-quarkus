package com.strength.application.port.in;

import com.strength.domain.GymClass;
import java.util.List;

public interface GetGymClassesUseCase {

  List<GymClass> findAll();
}
