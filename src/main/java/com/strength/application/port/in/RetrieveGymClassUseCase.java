package com.strength.application.port.in;

import com.strength.domain.GymClass;

public interface RetrieveGymClassUseCase {

  GymClass findById(Long id);
}
