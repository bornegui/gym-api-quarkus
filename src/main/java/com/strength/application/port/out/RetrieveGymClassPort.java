package com.strength.application.port.out;

import com.strength.domain.GymClass;

public interface RetrieveGymClassPort {

  GymClass findById(Long id);
}
