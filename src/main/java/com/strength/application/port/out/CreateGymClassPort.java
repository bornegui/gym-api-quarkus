package com.strength.application.port.out;

import com.strength.domain.GymClass;

public interface CreateGymClassPort {

  void createGymClass(GymClass gymClass);
}
