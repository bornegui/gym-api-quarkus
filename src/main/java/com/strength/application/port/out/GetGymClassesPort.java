package com.strength.application.port.out;

import com.strength.domain.GymClass;
import java.util.List;

public interface GetGymClassesPort {

  List<GymClass> findAll();
}
