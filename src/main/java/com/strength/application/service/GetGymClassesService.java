package com.strength.application.service;

import com.strength.application.port.in.GetGymClassesUseCase;
import com.strength.application.port.out.GetGymClassesPort;
import com.strength.domain.GymClass;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GetGymClassesService implements GetGymClassesUseCase {

  @Inject
  GetGymClassesPort getGymClassesPort;

  @Override
  public List<GymClass> findAll() {
    return getGymClassesPort.findAll();
  }
}
