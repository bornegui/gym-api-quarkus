package com.strength.application.service;

import com.strength.application.port.in.CreateGymClassUseCase;
import com.strength.application.port.out.CreateGymClassPort;
import com.strength.domain.GymClass;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
@Transactional
public class CreateGymClassService implements CreateGymClassUseCase {

  @Inject
  CreateGymClassPort createGymClassPort;

  @Override
  public void createGymClass(GymClass gymClass) {
    createGymClassPort.createGymClass(gymClass);
  }

}
