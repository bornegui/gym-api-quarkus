package com.strength.application.service;

import com.strength.application.port.in.RetrieveGymClassUseCase;
import com.strength.application.port.out.RetrieveGymClassPort;
import com.strength.domain.GymClass;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class RetrieveGymClassService implements RetrieveGymClassUseCase {

  @Inject
  RetrieveGymClassPort retrieveGymClassPort;

  @Override
  public GymClass findById(Long id) {
    return retrieveGymClassPort.findById(id);
  }
}
