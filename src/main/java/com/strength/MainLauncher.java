package com.strength;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain
public class MainLauncher {
  /**
   * Launch the app in web or batch mode.
   *
   * @param args the app's arguments
   */
  public static void main(String... args) {
    Quarkus.run(args);
  }

}

